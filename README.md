# Java 体系化成长

[![知识共享协议（CC协议）](https://img.shields.io/badge/License-Creative%20Commons-DC3D24.svg)](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)
[![GitHub stars](https://img.shields.io/github/stars/hbulpf/JavaPrinciple.svg?label=Stars)](https://github.com/hbulpf/JavaPrinciple)
[![GitHub watchers](https://img.shields.io/github/watchers/hbulpf/JavaPrinciple.svg?label=Watchers)](https://github.com/hbulpf/JavaPrinciple/watchers)
[![GitHub forks](https://img.shields.io/github/forks/hbulpf/JavaPrinciple.svg?label=Forks)](https://github.com/hbulpf/JavaPrinciple/fork)

# Linux基础

- [Linux](knowledge/Linux/README.md)
    - [Linux虚拟机](knowledge/Linux/笔记2-Linux虚拟机.md)
    - [X-Shell命令](knowledge/Linux/笔记3-X-shell.md)
    - [Linux文件系统相关的命令](knowledge/Linux/笔记4-Linux-文件系统相关的命令.md)
    - [正则表达式](knowledge/Linux/笔记5-正则表达式.md)
    - [文本数据处理命令](knowledge/Linux/笔记6-文本数据处理命令.md)
    - [Linux软件安装](knowledge/Linux/笔记7-Linux软件安装.md)

# Java知识
- [JAVA架构师之路](knowledge/JAVA架构师之路.md)
- [J2SE基础](knowledge/j2se/README.md)
    - [Java集合](knowledge/j2se/Java集合.md)
        - [Java队列](knowledge/j2se/Java队列.md)
        - [Java栈](knowledge/j2se/Java栈.md)
        - [PriorityQueue](knowledge/j2se/PriorityQueue.md)
        - [List和Array用法](knowledge/j2se/List和Array用法.md)
    - [Java工具类](knowledge/j2se/utils/README.md)
        - [BeanUtils](knowledge/j2se/utils/BeanUtils.md)
        - [Date类](knowledge/j2se/utils/Date.md)
        - [常见加密算法](knowledge/j2se/utils/encryption.md)
        - [RSA数字证书生成](knowledge/j2se/utils/RSA数字证书生成.md)
    - [Java的classpath指什么](knowledge/j2se/Java的classpath指什么.md)
    - [java和javaw的区别](knowledge/j2se/java和javaw的区别.md)
    - [线程池](knowledge/j2se/threadpool.md)
    - [常用开发技巧](knowledge/j2se/tips.md)
    - [常用工具类](knowledge/j2se/常用工具类.md)
- [Spring](knowledge/spring/README.md)
  - [spring框架](knowledge/spring/springframework/README.md)
    - [Spring常用注解总结](knowledge/spring/springframework/Spring常用注解总结.md)
    - [CrossOrigin允许跨域](knowledge/spring/springframework/CrossOrigin允许跨域.md)
    - [springtest](knowledge/spring/springtest.md)
  - [SpringBoot](knowledge/spring/springboot/README.md)
    - [springboot同时支持http和https访问](knowledge/spring/springboot/springboot同时支持http和https访问.md)
    - [SpringBoot使用腾讯云SSL证书并开启HTTPS访问](knowledge/spring/springboot/SpringBoot使用腾讯云SSL证书并开启HTTPS访问.md)
  - [SpringCloud](knowledge/spring/springcloud/README.md)
- [框架工具](knowledge/框架工具/README.md)
    - [logback](knowledge/框架工具/logback/README.md)
    - [Maven](knowledge/框架工具/maven/mavencmd.md)
        - [pom介绍](knowledge/框架工具/maven/maven_pom.md)
    - [Mybatis](knowledge/框架工具/mybatis.md)
    - [Lambda表达式](knowledge/java8/Lambda.md)
- [Tomcat](knowledge/tomcat.md)
- [JVM](knowledge/jvm/jvm.md)
    - [JVM基础工具](knowledge/jvm/jvmtools_1_基础工具.md)
    - [JVM: arthas](knowledge/jvm/jvmtools_2_arthas.md)
    - [Arthas活用ognl表达式](knowledge/jvm/jvmtools_3_ognl.md)
    - [JVM1:内存组成](knowledge/jvm/jvm1.md)
    - [JVM2:HotSpot垃圾收集器的种类](knowledge/jvm/jvm2.md)
    - [JVM3:GC策略&内存申请、对象衰老](knowledge/jvm/jvm3.md)
    - [JVM4:JVM参数优化设置](knowledge/jvm/jvm4.md)
    - [JVM5:生产环境实例分析](knowledge/jvm5.md)
    - [JVM6:JVM监测工具](knowledge/jvm/jvm6.md)
    - [JVM7:classLoader的卸载机制](knowledge/jvm/jvm7.md)
    - [JVM8:Dynamic Code Evolution for Java dcevm原理](knowledge/jvm/jvm8.md)
    - [一文看懂JVM的GC](knowledge/jvm/一文看懂JVM的GC.md)  
- [面试题](knowledge/面试题.md)


# 算法

- [数据结构与算法](knowledge/算法/README.md)
    - [五大常用算法](knowledge/算法/五大常用算法.md)
    - [oj](knowledge/算法/oj.md)
    - [笔试练习题](knowledge/算法/笔试练习题.md)
- [leetcode](knowledge/leetcode/README.md)


# 设计模式

- [设计模式](knowledge/设计模式/README.md)
  - [单例模式](knowledge/设计模式/singleton.md)
  - [工厂方法模式](knowledge/设计模式/factorymethod.md)
  - [抽象工厂模式](knowledge/设计模式/abstractfactory.md)
  - [模板方法模式](knowledge/设计模式/model.md)
  - [代理模式](knowledge/设计模式/proxy.md)
  - [适配器模式](knowledge/设计模式/adapter.md)
  - [建造者模式](knowledge/设计模式/builder.md)
  - [观察者模式](knowledge/设计模式/observer.md)
  - [原型模式](knowledge/设计模式/prototype.md)


# 其他

- [原型开发](knowledge/prototype.md)

  
# 推荐

- [书单推荐](BookList.md)




----------------------------------------

**项目规范**

本文使用 [`Markdown`](https://www.markdownguide.org/basic-syntax) 编写, 排版符合[`中文技术文档写作规范`](https://github.com/hbulpf/document-style-guide)。Find Me On [**Github**](https://github.com/hbulpf/JavaPrinciple) , [**Gitee**](https://gitee.com/sifangcloud/JavaPrinciple)

**友情贡献**

@[**RunAtWorld**](http://www.github.com/RunAtWorld) &nbsp;@[**iceyung**](https://github.com/iceyung) 

#### 关注公众号 『四方云和』 获取更多优质文章 ~

![sfyh_qrcode](sfyh_qrcode.jpg)