# 设计模式

1. [单例模式](singleton.md)
2. [工厂方法模式](factorymethod.md)
3. [抽象工厂模式](abstractfactory.md)
4. [模板方法模式](model.md)
5. [代理模式](proxy.md)
6. [适配器模式](adapter.md)
7. [建造者模式](builder.md)
8. [观察者模式](observer.md)
9. [原型模式](prototype.md)
